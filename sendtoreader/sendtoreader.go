package sendtoreader

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/pkg/errors"
)

var apiURL = "https://sendtoreader.com/api"

type apiMethod string

const (
	checkLoginMethod apiMethod = "checklogin"
	sendMethod                 = "send"
)

const (
	usernameParam = "username"
	passwordParam = "password"
	urlParam      = "url"
)

type Client struct {
	Username string
	Password string
	Client   *http.Client
}

func (c Client) CheckLogin() error {
	return c.call(checkLoginMethod, nil)
}

func (c Client) SendURL(u string) error {
	return c.call(sendMethod, url.Values{
		urlParam: []string{u},
	})
}

func (c Client) call(method apiMethod, values url.Values) error {
	u := fmt.Sprintf("%s/%s/", apiURL, method)

	allValues := url.Values{}
	allValues.Set(usernameParam, c.Username)
	allValues.Set(passwordParam, c.Password)

	for key, val := range values {
		for _, v := range val {
			allValues.Add(key, v)
		}
	}

	response, err := c.Client.PostForm(u, allValues)
	if err != nil {
		return err
	}
	response.Body.Close()

	switch response.StatusCode {
	case http.StatusOK:
		return nil
	case http.StatusUnauthorized:
		return errors.New("exceeded the rate limit; please wait 10 seconds and try again")
	case http.StatusForbidden:
		return errors.New("invalid username or password")
	case http.StatusMethodNotAllowed:
		return errors.New("sendtoreader is not able to process this URL")
	case http.StatusNotAcceptable:
		return errors.New("specified URL is not correct")
	case http.StatusFailedDependency:
		return errors.New("internal sendtoreader error; please contact support for investigation")
	case 427:
		return errors.New("sendtoreader can't send document to the user; maybe wrong or no kindle email in this user's settings")
	case http.StatusInternalServerError:
		return errors.New("the service encountered an unspecified internal error; please try again later")
	default:
		return errors.Errorf("received unknown response status: %v" + response.Status)
	}
}
