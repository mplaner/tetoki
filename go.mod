module gitlab.com/mplaner/tetoki

go 1.14

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.9.0
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/tucnak/telebot.v2 v2.4.0
)
