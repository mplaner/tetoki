FROM golang:alpine AS build-env

WORKDIR /go/src/gitlab.com/mplaner/tetoki

RUN apk --no-cache add git curl

COPY . .

ENV GOOS=linux
ENV GOARCH=amd64
ENV CGO_ENABLED=0

RUN go build -ldflags="-s -w"


FROM alpine

RUN apk --no-cache add ca-certificates

COPY --from=build-env /go/src/gitlab.com/mplaner/tetoki/tetoki /tetoki

USER nobody
ENTRYPOINT [ "/tetoki" ]
