package main

import (
	"reflect"
	"testing"
)

func Test_findURLs(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want []string
	}{
		{
			name: "simple URL",
			s:    "http://google.com",
			want: []string{"http://google.com"},
		},
		{
			name: "multiple URLs",
			s:    "Hey ha https://medium.com/@selvaganesh93/diy-how-to-build-own-paas-by-yourself-self-hosted-heroku-architecture-on-ubuntu-3a4a6f4c4f5\n\nWie geht's\n\nhttps://blog.golang.org/ismmkeynote\n\nHier",
			want: []string{"https://medium.com/@selvaganesh93/diy-how-to-build-own-paas-by-yourself-self-hosted-heroku-architecture-on-ubuntu-3a4a6f4c4f5", "https://blog.golang.org/ismmkeynote"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findURLs(tt.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("findURLs() = %q, want %v", got, tt.want)
			}
		})
	}
}
