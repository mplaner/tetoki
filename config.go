package main

import (
	"errors"
	"log"
	"reflect"
	"strings"

	"github.com/spf13/viper"
)

const configPrefix = "tetoki"

// Config holds all the configuration options for tetoki.
type Config struct {
	SendToReaderUsername string
	SendToReaderPassword string

	TelegramBotToken     string
	TelegramAllowedUsers []int
}

// Validate checks the config for inconsistencies and mandatory fields.
func (c *Config) Validate() (*Config, error) {
	return c, func() error {
		if len(c.SendToReaderUsername) == 0 {
			return errors.New("sendtoreader username is required")
		}

		if len(c.SendToReaderPassword) == 0 {
			return errors.New("sendtoreader password is required")
		}

		if len(c.TelegramBotToken) == 0 {
			return errors.New("telegram bot token is required")
		}

		return nil
	}()
}

// LoadConfig loads the config from config files and environment variables.
func LoadConfig() (*Config, error) {
	viper.AutomaticEnv()
	viper.SetEnvPrefix(configPrefix)

	viper.SetConfigName(configPrefix) // name of config file (without extension)
	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME/." + configPrefix)

	if err := viper.ReadInConfig(); err != nil {
		switch err.(type) {
		case viper.ConfigFileNotFoundError:
			// just log missing config file and continue
			log.Print(err)
		default:
			return nil, err
		}
	}

	config := &Config{}
	bindEnvs(config)
	if err := viper.Unmarshal(config); err != nil {
		return nil, err
	}

	return config.Validate()
}

func bindEnvs(iface interface{}) {
	typ := reflect.TypeOf(iface).Elem()
	for i := 0; i < typ.NumField(); i++ {
		t := typ.Field(i)
		name := strings.ToLower(t.Name)
		tag, ok := t.Tag.Lookup("mapstructure")
		if ok {
			name = tag
		}
		viper.BindEnv(name)
	}
}
