package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"sync"
	"syscall"
	"time"

	"gitlab.com/mplaner/tetoki/sendtoreader"
	tb "gopkg.in/tucnak/telebot.v2"
)

type urlRequest struct {
	URL  string
	User *tb.User
}

func main() {
	config, err := LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	customTransport := http.DefaultTransport.(*http.Transport).Clone()
	customTransport.TLSClientConfig = &tls.Config{
		InsecureSkipVerify: true,
	}
	httpClient := &http.Client{
		Timeout:   30 * time.Second,
		Transport: customTransport,
	}

	client := sendtoreader.Client{
		Username: config.SendToReaderUsername,
		Password: config.SendToReaderPassword,
		Client:   httpClient,
	}

	if err := client.CheckLogin(); err != nil {
		log.Fatal(err)
	}
	log.Println("SENDtoREADER init successful")

	bot, err := tb.NewBot(tb.Settings{
		Token:  config.TelegramBotToken,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
		Client: httpClient,
		Reporter: func(err error) {
			// skip useless reports
			if !strings.Contains(err.Error(), "getUpdates() failed") {
				log.Printf("telegram error: %v", err)
			}
		},
	})

	if err != nil {
		log.Fatal(err)
	}
	log.Println("Telegram bot init successful")

	var wg sync.WaitGroup
	quitChan := make(chan struct{})
	urlChan := make(chan urlRequest, 10)

	wg.Add(1)
	go func() {
	L:
		for {
			select {
			case req := <-urlChan:
				if err := client.SendURL(req.URL); err != nil {
					log.Printf("could not send url via sendtoreader: %v", err)
					bot.Send(req.User, fmt.Sprintf("Could not process %q: %v", req.URL, err))
					continue
				}

				bot.Send(req.User, fmt.Sprintf("Processed: %s", req.URL))
			case <-quitChan:
				break L
			}
		}
		wg.Done()
	}()

	bot.Handle(tb.OnText, func(m *tb.Message) {
		if !isAllowed(m.Sender, config.TelegramAllowedUsers) {
			bot.Send(m.Sender, "unauthorized user: go away!")
			return
		}

		urls := findURLs(m.Text)
		for _, url := range urls {
			req := urlRequest{
				User: m.Sender,
				URL:  url,
			}

			select {
			case urlChan <- req:
				continue
			case <-time.After(3 * time.Second):
				log.Printf("timeout while inserting url request: %v", req)
				continue
			case <-quitChan:
				return
			}
		}

		if len(urls) > 0 {
			bot.Send(m.Sender, fmt.Sprintf("Found %d URLs. Processing...", len(urls)))
		} else {
			bot.Send(m.Sender, "No URLs found in message.")
		}
	})

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-sig
		close(quitChan)
		fmt.Println("Shutting down...")
		bot.Stop()
	}()

	bot.Start()
	wg.Wait()
}

var urlPattern = regexp.MustCompile(`(?mi:https?://[^" (\r\n)]+)`)

func findURLs(s string) []string {
	return urlPattern.FindAllString(s, -1)
}

func isAllowed(user *tb.User, allowedUsers []int) bool {
	if len(allowedUsers) == 0 {
		// accept all messages if no allowed users are set in config
		return true
	}

	for _, id := range allowedUsers {
		if user.ID == id {
			return true
		}
	}

	return false
}
